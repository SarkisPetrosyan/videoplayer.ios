//
//  ABMVolumeSlider.swift
//  ABMVideoPlayer
//
//  Created by Ruben Nahatakyan on 8/27/19.
//  Copyright © 2019 Ruben Nahatakyan. All rights reserved.
//

import UIKit
import AVKit
import MediaPlayer

public class ABMVolumeSlider: UIView {

    // MARK: - Views
    fileprivate var imageView: UIImageView!
    fileprivate var slider: ABMSlider!

    fileprivate var volumeView: MPVolumeView!

    // MARK: - Properties
    fileprivate var workItem: DispatchWorkItem?
    fileprivate var hideInPortrait: Bool = false

    // MARK: - Init
    override public init(frame: CGRect) {
        super.init(frame: frame)
        startupSetup()
    }

    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        startupSetup()
    }

    override public func layoutSubviews() {
        super.layoutSubviews()
        setCurrentVolume()
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}

// MARK: - Startup
extension ABMVolumeSlider {
    fileprivate func startupSetup() {
        setupUI()
        hideDefaultVolumeView()

        addObservers()

        hideVolumeSlider(animation: false)
        changeOrientation(self)
        isUserInteractionEnabled = false
    }

    fileprivate func addObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(volumeDidChange(_:)), name: NSNotification.Name(rawValue: "AVSystemController_SystemVolumeDidChangeNotification"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(changeOrientation(_:)), name: UIDevice.orientationDidChangeNotification, object: nil)
    }

    fileprivate func setCurrentVolume() {
        let audioSession = AVAudioSession.sharedInstance()
        do {
            try audioSession.setActive(true)
            slider.setProgress(CGFloat(audioSession.outputVolume), animation: false)
        } catch {
            slider.setProgress(0.5, animation: false)
            debugPrint(error.localizedDescription)
        }
    }

    fileprivate func hideDefaultVolumeView() {
        volumeView = MPVolumeView(frame: .zero)
        addSubview(volumeView)

        volumeView.alpha = 0.00001
        volumeView.isHidden = false
        volumeView.isUserInteractionEnabled = false
    }
}

// MARK: - Setup UI
extension ABMVolumeSlider {
    fileprivate func setupUI() {
        addImageView()
        addSlider()
    }

    fileprivate func addImageView() {
        imageView = UIImageView(frame: .zero)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(imageView)

        imageView.tintColor = .white
        imageView.contentMode = .scaleAspectFit
        let bundle = Bundle(for: ABMVolumeSlider.self)
        let image = UIImage(named: "ic_volumefull", in: bundle, compatibleWith: nil)
        imageView.image = image?.withRenderingMode(.alwaysTemplate)

        imageView.addAspectRatioConstraint()
        imageView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        imageView.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        imageView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
    }

    fileprivate func addSlider() {
        slider = ABMSlider(frame: .zero)
        slider.translatesAutoresizingMaskIntoConstraints = false
        addSubview(slider)

        slider.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        slider.heightAnchor.constraint(equalToConstant: 1.5).isActive = true
        slider.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        slider.leftAnchor.constraint(equalTo: imageView.rightAnchor, constant: 5).isActive = true
    }
}

// MARK: - Actions
extension ABMVolumeSlider {
    @objc func volumeDidChange(_ notification: NSNotification) {
        guard let volume = notification.userInfo?["AVSystemController_AudioVolumeNotificationParameter"] as? Float else { return }
        showVolumeSlider()
        let name = (volume == 0 ? "ic_volumefullno" : "ic_volumefull")
        let bundle = Bundle(for: ABMVolumeSlider.self)
        let image = UIImage(named: name, in: bundle, compatibleWith: nil)
        imageView.image = image?.withRenderingMode(.alwaysTemplate)
        slider.setProgress(CGFloat(volume))
    }

    @objc fileprivate func changeOrientation(_ sender: Any) {
        guard hideInPortrait else { return }
        switch UIApplication.shared.statusBarOrientation {
        case .landscapeLeft, .landscapeRight:
            isHidden = false
            volumeView.isHidden = false
        case .portrait, .portraitUpsideDown:
            isHidden = true
            volumeView.isHidden = true
        default:
            break
        }
    }
}

// MARK: - Public methods
extension ABMVolumeSlider {
    public func setHiddenOnPortrait(_ hidden: Bool) {
        hideInPortrait = hidden
        changeOrientation(self)
    }
}

// MARK: - Animations
extension ABMVolumeSlider {
    fileprivate func showVolumeSlider() {
        workItem?.cancel()

        UIView.animate(withDuration: Constants.animationDuration) {
            self.alpha = 1
        }

        workItem = DispatchWorkItem(block: { [weak self] in
            self?.hideVolumeSlider()
        })
        if let work = workItem {
            DispatchQueue.main.asyncAfter(deadline: .now() + 2, execute: work)
        }
    }

    fileprivate func hideVolumeSlider(animation: Bool = true) {
        if animation {
            UIView.animate(withDuration: Constants.animationDuration) {
                self.alpha = 0.0001
            }
        } else {
            alpha = 0.0001
        }
    }
}
