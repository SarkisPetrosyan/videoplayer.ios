//
//  VideoView.swift
//  ABMVideoPlayer
//
//  Created by Ruben Nahatakyan on 8/22/19.
//  Copyright © 2019 Ruben Nahatakyan. All rights reserved.
//

import UIKit
import AVKit

public class VideoView: UIImageView {

    // MARK: - Views
    fileprivate var currentTimeLabelBackground: CurrentTimeLabel!

    // MARK: - Properties
    fileprivate var playerLayer: AVPlayerLayer!
    fileprivate var sessionTask: URLSessionDataTask?
    public fileprivate(set) var streamingUrl: URL?

    fileprivate var playerManager: VideoPlayerManager {
        return VideoPlayerManager.shared
    }

    // MARK: - Public properties
    public var showCurrentTime: Bool = true {
        didSet {
            if showCurrentTime {
                addCurrentTimeLabel()
            } else {
                currentTimeLabelBackground?.removeFromSuperview()
                currentTimeLabelBackground = nil
            }
        }
    }

    // MARK: - Init
    override public init(frame: CGRect) {
        super.init(frame: frame)
        startupSetup()
    }

    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        startupSetup()
    }

    override public func layoutSubviews() {
        super.layoutSubviews()
        playerLayer?.frame = bounds
    }
}

// MARK: - Startup
extension VideoView {
    fileprivate func startupSetup() {
        setupUI()
        clipsToBounds = true
        contentMode = .scaleAspectFill
    }
}

// MARK: - Setup UI
extension VideoView {
    fileprivate func setupUI() {
        addCurrentTimeLabel()
    }

    fileprivate func addCurrentTimeLabel() {
        guard currentTimeLabelBackground == nil else { return }
        currentTimeLabelBackground = CurrentTimeLabel(frame: .zero)
        currentTimeLabelBackground.translatesAutoresizingMaskIntoConstraints = false
        addSubview(currentTimeLabelBackground)

        currentTimeLabelBackground.topAnchor.constraint(equalTo: topAnchor, constant: 10).isActive = true
        currentTimeLabelBackground.rightAnchor.constraint(equalTo: rightAnchor, constant: -10).isActive = true
        currentTimeLabelBackground.leftAnchor.constraint(greaterThanOrEqualTo: leftAnchor, constant: 5).isActive = true
    }
}

// MARK: - Actions
extension VideoView {
    fileprivate func setImage(path: String?) {
        self.image = nil
        let identifier = UUID().uuidString
        guard let imgPath = path, let url = URL(string: imgPath) else { return }

        self.accessibilityIdentifier = identifier

        DispatchQueue.global(qos: .background).async {
            var request = URLRequest(url: url)
            request.cachePolicy = URLRequest.CachePolicy.returnCacheDataElseLoad
            self.sessionTask = URLSession.shared.dataTask(with: request) { (data, response, error) in
                guard let data = data, error == nil else {
                    debugPrint(error?.localizedDescription ?? "Unkonwn error")
                    return
                }
                let image = UIImage(data: data)
                DispatchQueue.main.async {
                    if self.accessibilityIdentifier == identifier {
                        self.image = image
                    }
                }
            }
            self.sessionTask?.resume()
        }
    }
}

// MARK: - Public methods
extension VideoView {
    public func setData(placeholder: String?, streaming: String?) {
        setImage(path: placeholder)
        currentTimeLabelBackground?.setDuration(seconds: -1)
        if let streamingPath = streaming {
            streamingUrl = URL(string: streamingPath)
        } else {
            streamingUrl = nil
        }
    }

    public func playVideo() {
        guard let url = streamingUrl else { return }

        playerLayer = AVPlayerLayer()
        playerLayer.videoGravity = .resizeAspectFill
        if let currentTime = currentTimeLabelBackground {
            layer.insertSublayer(playerLayer, below: currentTime.layer)
        } else {
            layer.addSublayer(playerLayer)
        }
        playerLayer?.frame = bounds

        playerManager.delegate = self
        playerManager.playStreaming(url: url, playerLayer: &playerLayer)
    }

    public func pauseVideo() {
        playerManager.pausePlaying()
    }

    public func reset() {
        currentTimeLabelBackground?.setDuration(seconds: -1)
        sessionTask?.cancel()
    }
}

// MARK: - Vide player manager delegate
extension VideoView: VideoPlayerManagerDelegate {
    public func videoPeriodicObserver(currentTime: CMTime) {
        currentTimeLabelBackground?.setDuration(seconds: currentTime.seconds)
    }
}
