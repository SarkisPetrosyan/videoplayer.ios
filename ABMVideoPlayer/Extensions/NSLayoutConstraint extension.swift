//
//  NSLayoutConstraint extension.swift
//  Twogather
//
//  Created by Ruben Nahatakyan on 8/10/19.
//  Copyright © 2019 ArmBoldMind. All rights reserved.
//

import UIKit

extension NSLayoutConstraint {
    func changeMultiplayer(to value: CGFloat) -> NSLayoutConstraint {
        return NSLayoutConstraint(item: self.firstItem!, attribute: self.firstAttribute, relatedBy: self.relation, toItem: self.secondItem, attribute: self.secondAttribute, multiplier: value, constant: self.constant)
    }
}
