//
//  VideoPlayButton.swift
//  ABMVideoPlayer
//
//  Created by Ruben Nahatakyan on 8/23/19.
//  Copyright © 2019 Ruben Nahatakyan. All rights reserved.
//

import UIKit

public class VideoPlayButton: UIButton {

    // MARK: - Properties
    public fileprivate(set) var playType: VideoPlayButtonTypeEnum = .play

    // MARK: - Init
    override init(frame: CGRect) {
        super.init(frame: frame)
        startupSetup()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        startupSetup()
    }
}

// MARK: - Startup
extension VideoPlayButton {
    fileprivate func startupSetup() {
        setupUI()
    }
}

// MARK: - Setup UI
extension VideoPlayButton {
    fileprivate func setupUI() {
        changeImage()
    }
}

// MARK: - Actions
extension VideoPlayButton {
    fileprivate func changeImage() {
        let name = playType == .play ? "ic_pause" : "ic_play"
        let bundle = Bundle(for: VideoPlayButton.self)
        let image = UIImage(named: name, in: bundle, compatibleWith: nil)
        tintColor = .white
        setImage(image?.withRenderingMode(.alwaysTemplate), for: .normal)
    }
}

// MARK: - Public
extension VideoPlayButton {
    public func changeType(to type: VideoPlayButtonTypeEnum) {
        self.playType = type
        changeImage()
    }
}

// MARK: - Helpers
public enum VideoPlayButtonTypeEnum {
    case play
    case pause

    mutating func changeType() {
        self = self == .play ? .pause : .play
    }
}
