
Pod::Spec.new do |spec|

  spec.name          = "ABMVideoPlayer"
  spec.version       = "0.0.12"
  spec.summary       = "ABMVideoPlayer is video player for fun"
  spec.homepage      = "https://www.armboldmind.com/"
  spec.license       = { :type => "MIT", :file => "LICENSE" }
  spec.author        = { "Ruben" => "rubennah@gmail.com" }
  spec.platform      = :ios, "11.0"

  spec.source        = { :git => "https://gitlab.com/SarkisPetrosyan/videoplayer.ios.git", :tag => "#{spec.version}" }
  spec.source_files  = "ABMVideoPlayer", "ABMVideoPlayer/**/*.swift"
  spec.resources     = "ABMVideoPlayer/*.xcassets"

  spec.swift_version = "5"


end
