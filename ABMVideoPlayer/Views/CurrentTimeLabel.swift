//
//  CurrentTimeLabel.swift
//  ABMVideoPlayer
//
//  Created by Ruben Nahatakyan on 8/27/19.
//  Copyright © 2019 Ruben Nahatakyan. All rights reserved.
//

import UIKit

class CurrentTimeLabel: UIView {

    // MARK: - Views
    fileprivate var currentTimeLabel: UILabel!

    // MARK: - Init
    override init(frame: CGRect) {
        super.init(frame: frame)
        startupSetup()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        startupSetup()
    }
}

// MARK: - Startup
extension CurrentTimeLabel {
    fileprivate func startupSetup() {
        addLabel()
        setDuration(seconds: -1)

        clipsToBounds = true
        layer.cornerRadius = 2
        isUserInteractionEnabled = false
        backgroundColor = UIColor.black.withAlphaComponent(0.8)
    }

    fileprivate func addLabel() {
        currentTimeLabel = UILabel(frame: .zero)
        currentTimeLabel.translatesAutoresizingMaskIntoConstraints = false
        addSubview(currentTimeLabel)

        currentTimeLabel.textColor = .white
        currentTimeLabel.font = UIFont.systemFont(ofSize: 10)

        currentTimeLabel.topAnchor.constraint(equalTo: topAnchor, constant: 3).isActive = true
        currentTimeLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: 5).isActive = true
        currentTimeLabel.rightAnchor.constraint(equalTo: rightAnchor, constant: -5).isActive = true
        currentTimeLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -3).isActive = true
    }
}

// MARK: - Actions
extension CurrentTimeLabel {
    fileprivate func getTimeString(from seconds: Double) -> String {
        let hours = Int(seconds / 3600)
        let minutes = Int(seconds / 60) % 60
        let seconds = Int(seconds.truncatingRemainder(dividingBy: 60))
        if hours > 0 {
            return String(format: "%02i:%02i:%02i", arguments: [hours, minutes, seconds])
        } else {
            return String(format: "%02i:%02i", arguments: [minutes, seconds])
        }
    }
}

// MARK: - Public methods
extension CurrentTimeLabel {
    public func setDuration(seconds: Double) {
        currentTimeLabel.text = getTimeString(from: seconds)
        isHidden = seconds <= 0
    }
}
