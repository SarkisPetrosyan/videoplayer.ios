//
//  VideoResizeButton.swift
//  ABMVideoPlayer
//
//  Created by Ruben Nahatakyan on 8/26/19.
//  Copyright © 2019 Ruben Nahatakyan. All rights reserved.
//

import UIKit

public class VideoResizeButton: UIButton {

    // MARK: - Properties
    fileprivate(set) var deviceOrientationType: VideoResizeButtonTypeEnum = .portrait

    // MARK: - Init
    override init(frame: CGRect) {
        super.init(frame: frame)
        startupSetup()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        startupSetup()
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}

// MARK: - Startup
extension VideoResizeButton {
    fileprivate func startupSetup() {
        addObservers()
        changeImage()
        addAction()
    }

    fileprivate func addObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(changeOrientation(_:)), name: UIDevice.orientationDidChangeNotification, object: nil)
    }

    fileprivate func addAction() {
        addTarget(self, action: #selector(buttonAction(_:)), for: .touchUpInside)
    }
}

// MARK: - Actions
extension VideoResizeButton {
    @objc fileprivate func changeOrientation(_ sender: Notification) {
        switch UIApplication.shared.statusBarOrientation {
        case .landscapeLeft, .landscapeRight:
            deviceOrientationType = .landscape
        case .portrait, .portraitUpsideDown:
            deviceOrientationType = .portrait
        default:
            deviceOrientationType = .portrait
        }
        changeImage()
    }

    @objc fileprivate func buttonAction(_ sender: VideoResizeButton) {
        deviceOrientationType.changeType()
        switch deviceOrientationType {
        case .landscape:
            let value = UIInterfaceOrientation.landscapeRight.rawValue
            UIDevice.current.setValue(value, forKey: "orientation")
        case .portrait:
            let value = UIInterfaceOrientation.portrait.rawValue
            UIDevice.current.setValue(value, forKey: "orientation")
        }
    }

    fileprivate func changeImage() {
        let name = deviceOrientationType == .portrait ? "ic_full" : "ic_notfull"
        let bundle = Bundle(for: VideoResizeButton.self)
        let image = UIImage(named: name, in: bundle, compatibleWith: nil)
        tintColor = .white
        setImage(image?.withRenderingMode(.alwaysTemplate), for: .normal)
    }
}

// MARK: - Public
extension VideoResizeButton {
    public func changeType(to type: VideoResizeButtonTypeEnum) {
        self.deviceOrientationType = type
        changeImage()
    }
}

// MARK: - Helpers
public enum VideoResizeButtonTypeEnum {
    case portrait
    case landscape

    mutating func changeType() {
        self = self == .portrait ? .landscape : .portrait
    }
}
