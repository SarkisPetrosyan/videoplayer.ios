//
//  VideoPlayerManager.swift
//  ABMVideoPlayer
//
//  Created by Ruben Nahatakyan on 8/22/19.
//  Copyright © 2019 Ruben Nahatakyan. All rights reserved.
//

import UIKit
import AVKit

@objc public protocol VideoPlayerManagerDelegate: class {
    @objc optional func playerItemStatusChanged(status: AVPlayerItem.Status)
    @objc optional func videoPeriodicObserver(currentTime: CMTime)
    @objc optional func videoPlayedToEnd()
}

public class VideoPlayerManager: NSObject {

    // MARK: - Controllers
    public var videoPlayerController: VideoPlayerController!

    // MARK: - Properties
    public weak var delegate: VideoPlayerManagerDelegate?

    fileprivate(set) var asset: AVAsset!
    public fileprivate(set) var videoPlayer: AVPlayer?
    fileprivate(set) var videoPlayerItem: AVPlayerItem?

    fileprivate(set) var currentStreamingURL: URL?

    // Key-value observing context
    fileprivate var playerItemContext = 0
    fileprivate let requiredAssetKeys = ["playable", "hasProtectedContent"]

    fileprivate var statusObserverAdded: Bool = false

    // MARK: - Static
    public static let shared = VideoPlayerManager()

    // MARK: - Init
    fileprivate override init() {
        super.init()
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}

// MARK: - Actions
extension VideoPlayerManager {
    fileprivate func addObservers() {
        videoPlayerItem?.addObserver(self, forKeyPath: #keyPath(AVPlayerItem.status), options: [.old, .new], context: &playerItemContext)
        statusObserverAdded = true

        NotificationCenter.default.removeObserver(self, name: Notification.Name.AVPlayerItemDidPlayToEndTime, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(playerNotifications(_:)), name: Notification.Name.AVPlayerItemDidPlayToEndTime, object: nil)

        // Add periodic observer
        let interval = CMTime(seconds: 0.01, preferredTimescale: CMTimeScale(NSEC_PER_SEC))
        let mainQueue = DispatchQueue.main

        videoPlayer?.addPeriodicTimeObserver(forInterval: interval, queue: mainQueue, using: { time in
            guard let currentTime = self.videoPlayer?.currentItem?.currentTime() else { return }
            self.delegate?.videoPeriodicObserver?(currentTime: currentTime)
        })
    }
}

// MARK: - Notification actions
extension VideoPlayerManager {
    override public func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey: Any]?, context: UnsafeMutableRawPointer?) {
        guard let currentItem = videoPlayerItem, let keyPath = keyPath else { return }

        switch keyPath {
        case #keyPath(AVPlayerItem.status):
            delegate?.playerItemStatusChanged?(status: currentItem.status)
        default:
            break
        }
    }

    @objc fileprivate func playerNotifications(_ sender: Notification) {
        switch sender.name {
        case Notification.Name.AVPlayerItemDidPlayToEndTime:
            debugPrint("AVPlayerItemDidPlayToEndTime")
            delegate?.videoPlayedToEnd?()
        default:
            break
        }
    }
}

// MARK: - Video player close delegate
extension VideoPlayerManager: VideoPlayerControllerClosingDelegate {
    public func playerClosed() {
        videoPlayerController = nil
    }
}

// MARK: - Public methods
extension VideoPlayerManager {
    public func playStreaming(url: URL, playerLayer: inout AVPlayerLayer) {
        currentStreamingURL = url
        videoPlayer?.replaceCurrentItem(with: nil)

        asset = AVAsset(url: url)
        videoPlayerItem = AVPlayerItem(asset: asset, automaticallyLoadedAssetKeys: requiredAssetKeys)
        videoPlayer = AVPlayer(playerItem: videoPlayerItem)
        addObservers()

        playerLayer.player = videoPlayer

        videoPlayerItem?.preferredForwardBufferDuration = 1
        videoPlayer?.automaticallyWaitsToMinimizeStalling = true

        videoPlayer?.play()

        do {
            try AVAudioSession.sharedInstance().setCategory(.playback)
        } catch {
            debugPrint(error.localizedDescription)
        }
    }

    public func continuePlaying() {
        videoPlayer?.play()
    }

    public func pausePlaying() {
        videoPlayer?.pause()
    }

    public func stopPlaying() {
        videoPlayer?.replaceCurrentItem(with: nil)
        if statusObserverAdded {
            videoPlayerItem?.removeObserver(self, forKeyPath: #keyPath(AVPlayerItem.status))
        }
        statusObserverAdded = false
    }

    public func showPlayer(url: URL) {
        guard let rootController = UIApplication.shared.keyWindow?.rootViewController else { return }
        videoPlayerController = VideoPlayerController(url: url)
        videoPlayerController.closeDelegate = self
        videoPlayerController.modalPresentationStyle = .overCurrentContext
        videoPlayerController.modalPresentationCapturesStatusBarAppearance = true
        videoPlayerController.videoPlayEndAction = .replay
        videoPlayerController.showTimeLabelInTop = false
        rootController.present(videoPlayerController, animated: true, completion: nil)
    }

    public func setVolume(value: Float) {
        videoPlayer?.volume = value
    }
}
