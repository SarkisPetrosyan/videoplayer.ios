//
//  ABMSlider.swift
//  ABMVideoPlayer
//
//  Created by Ruben Nahatakyan on 8/27/19.
//  Copyright © 2019 Ruben Nahatakyan. All rights reserved.
//

import UIKit

class ABMSlider: UIView {

    // MARK: - Views
    fileprivate var progressView: UIView!

    // MARK: - Constraints
    fileprivate var progressWidthConst: NSLayoutConstraint!

    // MARK: - Properties
    fileprivate var minValue: CGFloat = 0
    fileprivate var maxValue: CGFloat = 1

    // MARK: - Init
    override init(frame: CGRect) {
        super.init(frame: frame)
        startupSetup()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        startupSetup()
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        let radius = frame.height / 2
        layer.cornerRadius = radius
        progressView.layer.cornerRadius = radius
    }
}

// MARK: - Startup
extension ABMSlider {
    fileprivate func startupSetup() {
        setupUI()
        clipsToBounds = true
        isUserInteractionEnabled = false
    }
}

// MARK: - Setup UI
extension ABMSlider {
    fileprivate func setupUI() {
        addProgressView()
        backgroundColor = UIColor(displayP3Red: 0.5, green: 0.5, blue: 0.5, alpha: 1)
    }

    fileprivate func addProgressView() {
        progressView = UIView(frame: .zero)
        progressView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(progressView)

        progressView.clipsToBounds = true
        progressView.backgroundColor = .white

        progressView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        progressView.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        progressView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true

        progressWidthConst = progressView.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.5)
        progressWidthConst.priority = UILayoutPriority(rawValue: 999)
        progressWidthConst.isActive = true
    }
}

// MARK: - Actions
extension ABMSlider {
    fileprivate func changeConstraintMultiplayer(constraint: inout NSLayoutConstraint, multiplayer: CGFloat) {
        let newConstraint = constraint.changeMultiplayer(to: multiplayer)
        removeConstraint(constraint)
        addConstraint(newConstraint)
        layoutIfNeeded()
        constraint = newConstraint
    }
}

// MARK: - Public methods
extension ABMSlider {
    public func setMaxProgress(_ value: CGFloat) {
        maxValue = value
    }

    public func setProgress(_ value: CGFloat, animation: Bool = true) {
        let multiplayer = max(value / self.maxValue, 0.001)
        if animation {
            UIView.animate(withDuration: Constants.animationDuration) {
                self.changeConstraintMultiplayer(constraint: &self.progressWidthConst, multiplayer: multiplayer)
            }
        } else {
            changeConstraintMultiplayer(constraint: &progressWidthConst, multiplayer: multiplayer)
        }
    }
}
