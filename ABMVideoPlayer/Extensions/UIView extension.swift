//
//  UIView extension.swift
//  ABMVideoPlayer
//
//  Created by Ruben Nahatakyan on 8/22/19.
//  Copyright © 2019 Ruben Nahatakyan. All rights reserved.
//

import UIKit

extension UIView {
    public func addAspectRatioConstraint(multiplayer: CGFloat = 1, priority: Float = 999) {
        let const = NSLayoutConstraint(item: self, attribute: .width, relatedBy: .equal, toItem: self, attribute: .height, multiplier: multiplayer, constant: 0)
        const.priority = UILayoutPriority(priority)
        const.isActive = true
    }

    public func showView(animation: Bool = true) {
        guard self.alpha == 0 else { return }
        self.isHidden = false
        if animation {
            UIView.animate(withDuration: Constants.animationDuration, animations: {
                self.alpha = 1
            })
        } else {
            self.alpha = 1
        }
    }

    public func hideView(animation: Bool = true) {
        guard self.alpha != 0 else { return }
        if animation {
            UIView.animate(withDuration: Constants.animationDuration, animations: {
                self.alpha = 0
            }) { (_) in
                self.isHidden = true
            }
        } else {
            self.alpha = 0
            self.isHidden = true
        }
    }
}
