//
//  VideoPlayerNavigationView.swift
//  ABMVideoPlayer
//
//  Created by Ruben Nahatakyan on 8/23/19.
//  Copyright © 2019 Ruben Nahatakyan. All rights reserved.
//

import UIKit

public class VideoPlayerNavigationView: UIView {

    // MARK: - Views
    public fileprivate(set) var playButton: VideoPlayButton!
    public fileprivate(set) var resizeButton: VideoResizeButton!
    public fileprivate(set) var stopButton: UIButton!
    public fileprivate(set) var timeLabel: UILabel!
    public fileprivate(set) var slider: UISlider!

    // MARK: - Static
    public static var defaultHeight: CGFloat = 40

    // MARK: - Init
    public override init(frame: CGRect) {
        super.init(frame: frame)
        startupSetup()
    }

    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        startupSetup()
    }
}

// MARK: - Startup
extension VideoPlayerNavigationView {
    fileprivate func startupSetup() {
        setupUI()
    }
}

// MARK: - Setup UI
extension VideoPlayerNavigationView {
    fileprivate func setupUI() {
        addPlayButton()
        addStopButton()
        addResizeVideoButton()
        addTimeLabel()
        addTimeSlider()

        backgroundColor = UIColor.black.withAlphaComponent(0.8)
    }

    fileprivate func addPlayButton() {
        playButton = VideoPlayButton(frame: .zero)
        playButton.translatesAutoresizingMaskIntoConstraints = false
        addSubview(playButton)

        let yConstant = VideoPlayerNavigationView.defaultHeight / 8
        playButton.addAspectRatioConstraint()
        playButton.heightAnchor.constraint(equalTo: safeAreaLayoutGuide.heightAnchor).isActive = true
        playButton.centerYAnchor.constraint(equalTo: safeAreaLayoutGuide.centerYAnchor).isActive = true
        playButton.leftAnchor.constraint(equalTo: safeAreaLayoutGuide.leftAnchor, constant: yConstant).isActive = true
    }

    fileprivate func addStopButton() {
        stopButton = VideoPlayButton(frame: .zero)
        stopButton.translatesAutoresizingMaskIntoConstraints = false
        addSubview(stopButton)

        let bundle = Bundle(for: VideoPlayerNavigationView.self)
        let image = UIImage(named: "ic_stop", in: bundle, compatibleWith: nil)
        stopButton.setImage(image, for: .normal)

        let yConstant = VideoPlayerNavigationView.defaultHeight / 8
        stopButton.addAspectRatioConstraint()
        stopButton.heightAnchor.constraint(equalTo: playButton.heightAnchor).isActive = true
        stopButton.centerYAnchor.constraint(equalTo: safeAreaLayoutGuide.centerYAnchor).isActive = true
        stopButton.leftAnchor.constraint(equalTo: playButton.rightAnchor, constant: yConstant).isActive = true
    }

    fileprivate func addResizeVideoButton() {
        resizeButton = VideoResizeButton(frame: .zero)
        resizeButton.translatesAutoresizingMaskIntoConstraints = false
        addSubview(resizeButton)

        let yConstant = VideoPlayerNavigationView.defaultHeight / 8
        resizeButton.addAspectRatioConstraint()
        resizeButton.heightAnchor.constraint(equalTo: playButton.heightAnchor).isActive = true
        resizeButton.centerYAnchor.constraint(equalTo: safeAreaLayoutGuide.centerYAnchor).isActive = true
        resizeButton.rightAnchor.constraint(equalTo: safeAreaLayoutGuide.rightAnchor, constant: -yConstant).isActive = true
    }

    fileprivate func addTimeLabel() {
        timeLabel = UILabel(frame: .zero)
        timeLabel.translatesAutoresizingMaskIntoConstraints = false
        addSubview(timeLabel)

        timeLabel.text = "00:00 / 00:00"
        timeLabel.textColor = .white
        timeLabel.textAlignment = .center
        timeLabel.font = .systemFont(ofSize: 14)
        timeLabel.adjustsFontSizeToFitWidth = true

        let yConstant = VideoPlayerNavigationView.defaultHeight / 8
        timeLabel.widthAnchor.constraint(equalToConstant: 95).isActive = true
        timeLabel.centerYAnchor.constraint(equalTo: safeAreaLayoutGuide.centerYAnchor).isActive = true
        timeLabel.rightAnchor.constraint(equalTo: resizeButton.leftAnchor, constant: -yConstant).isActive = true
    }

    fileprivate func addTimeSlider() {
        slider = UISlider(frame: .zero)
        slider.translatesAutoresizingMaskIntoConstraints = false
        addSubview(slider)

        slider.tintColor = .white
        slider.maximumTrackTintColor = .white
        slider.minimumTrackTintColor = .white

        let yConstant = VideoPlayerNavigationView.defaultHeight / 8
        slider.centerYAnchor.constraint(equalTo: safeAreaLayoutGuide.centerYAnchor).isActive = true
        slider.leftAnchor.constraint(equalTo: stopButton.rightAnchor, constant: yConstant).isActive = true
        slider.rightAnchor.constraint(equalTo: timeLabel.leftAnchor, constant: -yConstant).isActive = true
    }
}

// MARK: - Public methods
extension VideoPlayerNavigationView {
    public func setDuration(duration: Double, currentTime: Double) {
        slider.minimumValue = 0
        slider.value = Float(currentTime)
        slider.maximumValue = Float(duration)

        timeLabel.text = "\(currentTime.getTimeString()) / \(duration.getTimeString())"
    }
}
