//
//  Double extension.swift
//  ABMVideoPlayer
//
//  Created by Ruben Nahatakyan on 8/23/19.
//  Copyright © 2019 Ruben Nahatakyan. All rights reserved.
//

import UIKit

extension Double {
    public func getTimeString() -> String {
        let hours = Int(self / 3600)
        let minutes = Int(self / 60) % 60
        let seconds = Int(self.truncatingRemainder(dividingBy: 60))
        if hours > 0 {
            return String(format: "%02i:%02i:%02i", arguments: [hours, minutes, seconds])
        } else {
            return String(format: "%02i:%02i", arguments: [minutes, seconds])
        }
    }
}
