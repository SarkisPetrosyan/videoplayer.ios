//
//  SoundMuteButton.swift
//  ABMVideoPlayer
//
//  Created by Ruben Nahatakyan on 8/28/19.
//  Copyright © 2019 Ruben Nahatakyan. All rights reserved.
//

import UIKit
import MediaPlayer

public class SoundMuteButton: UIButton {

    // MARK: - Properties
    fileprivate(set) var soundType: SoundButtonTypeEnum = .volume {
        didSet {
            let bundle = Bundle(for: SoundMuteButton.self)
            let image = UIImage(named: soundType.rawValue, in: bundle, compatibleWith: nil)
            setImage(image, for: .normal)
        }
    }

    // MARK: - Init
    override public init(frame: CGRect) {
        super.init(frame: frame)
        startupSetup()
    }

    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        startupSetup()
    }
}

// MARK: - Startup
extension SoundMuteButton {
    fileprivate func startupSetup() {
        addTarget(self, action: #selector(buttonAction(_:)), for: .touchUpInside)

        clipsToBounds = true
        layer.cornerRadius = 2
        soundType = .volume

        imageEdgeInsets = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
    }
}

// MARK: - Actions
extension SoundMuteButton {
    @objc fileprivate func buttonAction(_ sender: SoundMuteButton) {
        soundType = (soundType == .volume) ? .mute : .volume
        changeVolume()
    }

    fileprivate func changeVolume() {
        VideoPlayerManager.shared.setVolume(value: (soundType == .volume) ? 1 : 0)
    }
}

// MARK: - Helpers
enum SoundButtonTypeEnum: String {
    case volume = "ic_volume"
    case mute = "ic_novolume"
}
