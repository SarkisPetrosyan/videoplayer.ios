//
//  VideoPlayerController.swift
//  ABMVideoPlayer
//
//  Created by Ruben Nahatakyan on 8/22/19.
//  Copyright © 2019 Ruben Nahatakyan. All rights reserved.
//

import UIKit
import AVKit
import MediaPlayer

@objc public protocol VideoPlayerControllerDelegate: class {
    @objc optional func videoPlayerClosed()
}

public protocol VideoPlayerControllerClosingDelegate: class {
    func playerClosed()
}

public class VideoPlayerController: UIViewController {

    // MARK: - Views
    fileprivate var videoBackground: UIView!
    fileprivate var controlButtonsBackground: VideoPlayerNavigationView!

    fileprivate var closeButton: UIButton!

    fileprivate var currentTimeLabel: CurrentTimeLabel!

    fileprivate var activityIndicator: UIActivityIndicatorView!

    fileprivate var controlViews: [UIView] = []

    // MARK: - Properties
    public weak var delegate: VideoPlayerControllerDelegate?
    weak var closeDelegate: VideoPlayerControllerClosingDelegate?

    fileprivate var streamUrl: URL!
    fileprivate var playerLayer: AVPlayerLayer!

    fileprivate var videoPlayer: AVPlayer? {
        return VideoPlayerManager.shared.videoPlayer
    }

    fileprivate var workItem: DispatchWorkItem?

    // Status bar
    fileprivate var statusBarHidden: Bool = false
    override public var preferredStatusBarUpdateAnimation: UIStatusBarAnimation {
        return .slide
    }

    override public var prefersStatusBarHidden: Bool {
        return statusBarHidden
    }

    override public var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .all
    }

    // Swipe down
    fileprivate var firstTouchLocation: CGPoint = .zero

    // MARK: - Public properties
    public var videoPlayEndAction: VideoPlayEndActionEnum = .pause
    public var showTimeLabelInTop: Bool = false

    // MARK: - Init
    init(url: URL) {
        self.streamUrl = url
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    // MARK: - Life cycle
    override public func viewDidLoad() {
        super.viewDidLoad()
        startupSetup()
    }

    override public func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        playerLayer.frame = videoBackground.bounds
    }

    override public func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        hideStatusBar()
    }

    override public func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        touchCanceledAction()
    }

    deinit {
        debugPrint("VideoPlayerController deinit")
    }
}

// MARK: - Startup
extension VideoPlayerController {
    fileprivate func startupSetup() {
        setupUI()
        setupPlayer()
        addUIActions()

        VideoPlayerManager.shared.delegate = self
        VideoPlayerManager.shared.playStreaming(url: streamUrl, playerLayer: &playerLayer)

        addObservers()
    }

    fileprivate func setupPlayer() {
        playerLayer = AVPlayerLayer()
        playerLayer.videoGravity = .resizeAspect
        videoBackground.layer.insertSublayer(playerLayer, at: 0)
        playerLayer.frame = videoBackground.bounds
    }

    fileprivate func addUIActions() {
        controlButtonsBackground.slider.addTarget(self, action: #selector(timeSliderAction(_: _:)), for: .valueChanged)
        controlButtonsBackground.playButton.addTarget(self, action: #selector(playButtonAction(_:)), for: .touchUpInside)
        controlButtonsBackground.stopButton.addTarget(self, action: #selector(stopButtonAction(_:)), for: .touchUpInside)
    }

    fileprivate func addObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(goToBackground(_:)), name: UIApplication.willResignActiveNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(becameFromBackground(_:)), name: UIApplication.didBecomeActiveNotification, object: nil)
    }
}

// MARK: - Setup UI
extension VideoPlayerController {
    fileprivate func setupUI() {
        addVideoBackground()
        addCloseButton()
        if showTimeLabelInTop {
            addCurrentTimeLabel()
        }
        addBottomView()
        addActivityIndicator()

        hideControlViews()
        addVolumeSlider()

        view.backgroundColor = .black
    }

    fileprivate func addVideoBackground() {
        videoBackground = UIView(frame: .zero)
        videoBackground.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(videoBackground)

        videoBackground.backgroundColor = .clear

        videoBackground.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        videoBackground.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        videoBackground.widthAnchor.constraint(lessThanOrEqualTo: view.widthAnchor).isActive = true
        videoBackground.heightAnchor.constraint(lessThanOrEqualTo: view.heightAnchor).isActive = true
//        videoBackground.addAspectRatioConstraint(multiplayer: 16 / 9, priority: 500)

        let heightAnch = videoBackground.heightAnchor.constraint(equalTo: view.heightAnchor)
        heightAnch.priority = .defaultHigh
        heightAnch.isActive = true

        let widthAnch = videoBackground.widthAnchor.constraint(equalTo: view.widthAnchor)
        widthAnch.priority = .defaultHigh
        widthAnch.isActive = true
    }

    fileprivate func addCloseButton() {
        // Top bar
        let topView = UIView(frame: .zero)
        topView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(topView)
        controlViews.append(topView)

        topView.backgroundColor = UIColor.black.withAlphaComponent(0.8)

        let height = VideoPlayerNavigationView.defaultHeight
        topView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        topView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        topView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        topView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: height).isActive = true

        // Close button
        closeButton = UIButton(frame: .zero)
        closeButton.translatesAutoresizingMaskIntoConstraints = false
        topView.addSubview(closeButton)
        controlViews.append(closeButton)

        let bundle = Bundle(for: VideoPlayerController.self)
        let image = UIImage(named: "ic_close", in: bundle, compatibleWith: nil)
        let closeImage = image?.withRenderingMode(.alwaysTemplate)
        closeButton.setImage(closeImage, for: .normal)
        closeButton.tintColor = .white
        closeButton.clipsToBounds = true
        closeButton.layer.cornerRadius = 5
        closeButton.backgroundColor = .clear
        closeButton.addTarget(self, action: #selector(closeButtonAction(_:)), for: .touchUpInside)

        let yConstant = VideoPlayerNavigationView.defaultHeight / 8
        closeButton.addAspectRatioConstraint()
        closeButton.heightAnchor.constraint(equalTo: topView.safeAreaLayoutGuide.heightAnchor).isActive = true
        closeButton.centerYAnchor.constraint(equalTo: topView.safeAreaLayoutGuide.centerYAnchor).isActive = true
        closeButton.leftAnchor.constraint(equalTo: topView.safeAreaLayoutGuide.leftAnchor, constant: yConstant).isActive = true
    }

    fileprivate func addCurrentTimeLabel() {
        currentTimeLabel = CurrentTimeLabel(frame: .zero)
        currentTimeLabel.translatesAutoresizingMaskIntoConstraints = false
        videoBackground.addSubview(currentTimeLabel)

        currentTimeLabel.topAnchor.constraint(equalTo: videoBackground.topAnchor, constant: 10).isActive = true
        currentTimeLabel.rightAnchor.constraint(equalTo: videoBackground.rightAnchor, constant: -10).isActive = true
        currentTimeLabel.leftAnchor.constraint(greaterThanOrEqualTo: closeButton.rightAnchor, constant: 5).isActive = true
    }

    fileprivate func addBottomView() {
        controlButtonsBackground = VideoPlayerNavigationView(frame: .zero)
        controlButtonsBackground.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(controlButtonsBackground)
        controlViews.append(controlButtonsBackground)

        let height = VideoPlayerNavigationView.defaultHeight
        controlButtonsBackground.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        controlButtonsBackground.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        controlButtonsBackground.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        controlButtonsBackground.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -height).isActive = true
    }

    fileprivate func addActivityIndicator() {
        activityIndicator = UIActivityIndicatorView(style: .white)
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(activityIndicator)

        activityIndicator.startAnimating()
        activityIndicator.hidesWhenStopped = true
        activityIndicator.isUserInteractionEnabled = false

        activityIndicator.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        activityIndicator.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        activityIndicator.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        activityIndicator.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
    }

    fileprivate func addVolumeSlider() {
        let slider = ABMVolumeSlider(frame: .zero)
        slider.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(slider)

        slider.heightAnchor.constraint(equalToConstant: 10).isActive = true
        slider.centerXAnchor.constraint(equalTo: view.safeAreaLayoutGuide.centerXAnchor).isActive = true
        slider.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 5).isActive = true
        slider.widthAnchor.constraint(equalTo: view.safeAreaLayoutGuide.widthAnchor, multiplier: 0.6).isActive = true
    }
}

// MARK: - UI Actions
extension VideoPlayerController {
    @objc fileprivate func closeButtonAction(_ sender: UIButton) {
        NotificationCenter.default.removeObserver(self)

        dismiss(animated: true) { [weak self] in
            VideoPlayerManager.shared.stopPlaying()
            self?.delegate?.videoPlayerClosed?()
            self?.closeDelegate?.playerClosed()
        }
    }

    @objc fileprivate func playButtonAction(_ sender: VideoPlayButton) {
        switch sender.playType {
        case .play:
            sender.changeType(to: .pause)
            VideoPlayerManager.shared.pausePlaying()
        case .pause:
            sender.changeType(to: .play)
            VideoPlayerManager.shared.continuePlaying()
        }
        addControlViewsWorkForHide()
    }

    @objc fileprivate func stopButtonAction(_ sender: UIButton) {
        controlButtonsBackground.playButton.changeType(to: .pause)
        VideoPlayerManager.shared.pausePlaying()
        let timeScale = videoPlayer?.currentTime().timescale ?? 1000
        videoPlayer?.seek(to: CMTimeMake(value: 0, timescale: timeScale))
        controlButtonsBackground.slider.value = 0

        addControlViewsWorkForHide()
    }

    @objc fileprivate func timeSliderAction(_ sender: UISlider, _ event: UIEvent) {
        guard let touch = event.allTouches?.first else { return }
        switch touch.phase {
        case .began:
            VideoPlayerManager.shared.pausePlaying()
        case .moved:
            let timeScale = videoPlayer?.currentTime().timescale ?? Int32(1000)
            let value = Int64(sender.value * Float(timeScale))
            let cmTime = CMTimeMake(value: value, timescale: timeScale)
            videoPlayer?.seek(to: cmTime)
        case .ended, .cancelled:
            VideoPlayerManager.shared.continuePlaying()
        case .stationary:
            debugPrint("stationary")
        default:
            debugPrint("UISlider touch event future case that aren't handled")
        }

        addControlViewsWorkForHide()
    }
}

// MARK: - Video manager delegate
extension VideoPlayerController: VideoPlayerManagerDelegate {
    public func playerItemStatusChanged(status: AVPlayerItem.Status) {
        switch status {
        case .failed:
            debugPrint("failed")
        case .readyToPlay:
            let videoRect = playerLayer.videoRect
            let ratio = videoRect.width / videoRect.height
            videoBackground.addAspectRatioConstraint(multiplayer: ratio, priority: 999)
        case .unknown:
            debugPrint("unknown")
        default:
            debugPrint("AVPlayer currentItem status future case that aren't handled")
        }
    }

    public func videoPeriodicObserver(currentTime: CMTime) {
        guard let player = videoPlayer, let currentItem = player.currentItem else { return }

        if currentItem.isPlaybackLikelyToKeepUp {
            hideLoadingIndicator()
        } else {
            showLoadingIndicator()
        }

        let programDuration = currentItem.duration.seconds
        guard !programDuration.isNaN else { return }

        // TODO: Bad way to fix slider seek issue
        if abs(currentTime.seconds - Double(controlButtonsBackground.slider.value)) < 2 {
            controlButtonsBackground.setDuration(duration: programDuration, currentTime: currentItem.currentTime().seconds)
            currentTimeLabel?.setDuration(seconds: currentTime.seconds)
        }
    }

    public func videoPlayedToEnd() {
        guard controlButtonsBackground.playButton.playType == .play else { return }

        switch videoPlayEndAction {
        case .pause:
            controlButtonsBackground.playButton.changeType(to: .pause)
            VideoPlayerManager.shared.pausePlaying()
            showControlViews()
        case .replay:
            stopButtonAction(controlButtonsBackground.stopButton)
            playButtonAction(controlButtonsBackground.playButton)
        case .stop:
            stopButtonAction(controlButtonsBackground.stopButton)
            showControlViews()
        case .close:
            closeButtonAction(closeButton)
        }
    }
}

// MARK: - Notification actions
extension VideoPlayerController {
    @objc fileprivate func goToBackground(_ sender: Notification) {
        touchCanceledAction()
        guard controlButtonsBackground.playButton.playType == .play else { return }
        VideoPlayerManager.shared.pausePlaying()
    }

    @objc fileprivate func becameFromBackground(_ sender: Notification) {
        guard controlButtonsBackground.playButton.playType == .play else { return }
        VideoPlayerManager.shared.continuePlaying()
    }
}

// MARK: - Touches actions
extension VideoPlayerController {
    override public func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        guard let touch = touches.first else { return }
        touchAction(touch)
    }

    override public func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesMoved(touches, with: event)
        guard let touch = touches.first else { return }
        touchAction(touch)
    }

    override public func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesEnded(touches, with: event)
        guard let touch = touches.first else { return }
        touchAction(touch)
    }

    fileprivate func touchAction(_ touch: UITouch) {
        let location = touch.location(in: view)
        switch touch.phase {
        case .began:
            firstTouchLocation = location
        case .moved:
            // Handle only swipe down
            guard firstTouchLocation != .zero, location.y > firstTouchLocation.y else {
                touchEndedAction(touch)
                return
            }
            touchMovedAction(touch)
        case .ended:
            touchEndedAction(touch)
        case .cancelled:
            touchCanceledAction()
        case .stationary:
            debugPrint("touch stationary")
        default:
            debugPrint("touch future case that aren't handled")
        }
    }

    fileprivate func touchMovedAction(_ touch: UITouch) {
        // Change view location
        let location = touch.location(in: view)
        let prevLocation = touch.previousLocation(in: view)
        let locationY = (location.y + prevLocation.y) / 2
        view.frame.origin.y = locationY - firstTouchLocation.y

        // Change view alpha
        let distance = location.y - firstTouchLocation.y
        let maxDistance = view.frame.height / 4
        let alpha = max(1 - distance / maxDistance, 0)
        view.alpha = alpha

        showStatusBar()
    }

    fileprivate func touchEndedAction(_ touch: UITouch) {
        guard firstTouchLocation != .zero else { return }

        let location = touch.location(in: view)
        if location == firstTouchLocation {
            // Tap action
            if controlButtonsBackground.alpha == 0 {
                showControlViews()
            } else {
                hideControlViews()
            }
        } else if location.y - firstTouchLocation.y > 50 {
            closeButtonAction(closeButton)
        } else {
            touchCanceledAction()
        }
        firstTouchLocation = .zero
    }

    fileprivate func touchCanceledAction() {
        hideStatusBar()
        UIView.animate(withDuration: Constants.animationDuration, animations: {
            self.view.frame = self.view.bounds
            self.view.alpha = 1
        })
    }
}

// MARK: - Animations
extension VideoPlayerController {
    fileprivate func showStatusBar() {
        guard statusBarHidden else { return }
        statusBarHidden = false
        UIView.animate(withDuration: Constants.animationDuration) {
            self.setNeedsStatusBarAppearanceUpdate()
        }
    }

    fileprivate func hideStatusBar() {
        guard statusBarHidden == false else { return }
        statusBarHidden = true
        UIView.animate(withDuration: Constants.animationDuration) {
            self.setNeedsStatusBarAppearanceUpdate()
        }
    }

    fileprivate func showControlViews() {
        if controlButtonsBackground.alpha == 0 {
            controlViews.forEach { $0.showView() }
        }

        addControlViewsWorkForHide()
    }

    fileprivate func addControlViewsWorkForHide() {
        workItem?.cancel()

        guard controlButtonsBackground.alpha != 0, controlButtonsBackground.playButton.playType == .play else { return }
        workItem = DispatchWorkItem(block: { [weak self] in
            self?.hideControlViews()
        })

        if let work = workItem {
            DispatchQueue.main.asyncAfter(deadline: .now() + 2, execute: work)
        }
    }

    fileprivate func hideControlViews() {
        if controlButtonsBackground.alpha != 0 {
            controlViews.forEach { $0.hideView() }
        }
        workItem?.cancel()
    }

    fileprivate func showLoadingIndicator() {
        guard activityIndicator.isAnimating == false else { return }
        activityIndicator.startAnimating()
    }

    fileprivate func hideLoadingIndicator() {
        guard activityIndicator.isAnimating else { return }
        activityIndicator.stopAnimating()
    }
}

// MARK: - Public methods
extension VideoPlayerController {

}

// MARK: - Helpers
public enum VideoPlayEndActionEnum {
    case replay
    case pause
    case stop
    case close
}
