//
//  Constants.swift
//  ABMVideoPlayer
//
//  Created by Ruben Nahatakyan on 8/23/19.
//  Copyright © 2019 Ruben Nahatakyan. All rights reserved.
//

import UIKit

final class Constants: NSObject {
    // MARK: - Init
    fileprivate override init() {
        super.init()
    }

    static let animationDuration: Double = 0.2
}
